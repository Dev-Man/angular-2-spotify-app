"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var Rx_1 = require('rxjs/Rx');
require('rxjs/add/operator/map');
var SPOTIFY_API_URL = 'https://api.spotify.com/v1';
var SpotifyService = (function () {
    function SpotifyService(_http) {
        this._http = _http;
        console.log('Spotify Service Started...');
    }
    SpotifyService.prototype.searchArtists = function (searchString) {
        return this._http.get(SPOTIFY_API_URL + '/search?q=' + searchString + '&offset=0&limit=20&type=artist&market=US')
            .map(function (res) { return res.json().artists.items; })
            .do(function (responseData) { console.log('SEARCH ARTISTS DATA...'); console.log(responseData); })
            .catch(this._handleError);
    };
    SpotifyService.prototype.getArtist = function (id) {
        return this._http.get(SPOTIFY_API_URL + '/artists/' + id)
            .map(function (res) { return res.json(); })
            .do(function (responseData) { console.log('GET ARTIST DATA...'); console.log(responseData); })
            .catch(this._handleError);
    };
    SpotifyService.prototype.getAlbums = function (id) {
        return this._http.get(SPOTIFY_API_URL + '/artists/' + id + '/albums')
            .map(function (res) { return res.json().items; })
            .do(function (responseData) { console.log('GET ALBUM DATA...'); console.log(responseData); })
            .catch(this._handleError);
    };
    SpotifyService.prototype.getAlbum = function (id) {
        return this._http.get(SPOTIFY_API_URL + '/albums/' + id)
            .map(function (res) { return res.json(); })
            .do(function (responseData) { console.log('GET ALBUM DATA...'); console.log(responseData); })
            .catch(this._handleError);
    };
    SpotifyService.prototype._handleError = function (err, caught) {
        console.log('Spotify Service:', err);
        return Rx_1.Observable.throw(err);
    };
    SpotifyService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], SpotifyService);
    return SpotifyService;
}());
exports.SpotifyService = SpotifyService;
//# sourceMappingURL=spotify.service.js.map